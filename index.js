/// <reference path="typings/main.d.ts" />

var express = require('express');

var app = express();
var port = process.env.PORT || 3000;

app.use('/assets', express.static(__dirname + '/public'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
})

app.listen(port);

console.log('Server listening on port ' + port);
